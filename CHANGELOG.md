# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.2.0] - 2022-10-07
### Added
- Command-line scripts mature (tested thoroughly on NCI gadi)

### Changed
- Removed dependency on ClickHouse in favour of DuckDB, a superior OLAP with the added advantage
  of directly using parquet files on the filesystem instead of requiring a running DB process

## [0.1.1] - 2022-05-26
### Added
- gnomAD 3.1.2 as variant reference background 

### Changed
- Support for GRCh38/hg38 VCFs only

### Removed
- Support for GRCh37/hg19 VCFs and gnomAD 2.1.1

## [0.1.0] - 2021-11-30
### Added
- Public portal with lab links

### Changed
- Code almost ready for review, except for planned removal of hardcoded paths

### Removed
- Any reference to private data
