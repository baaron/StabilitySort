# Glossary

## Missense SNV table

| Column Name | Description |
| ------ | ------ |
| gene | HGNC gene symbol |
| aa_ref | amino-acid reference allele |
| aa_pos | amino-acid position |
| aa_alt | amino-acid alternate allele |
| zygosity | Homozygous or Heterozygous in sample |
| ddg | ΔΔG, Change in Gibbs free-energy, computed by Maestro |
| z_score | StabilitySort ΔΔG Z-score against gnomAD background |
| p_value | p-value of StabilitySort Z-score |
| oe_lof_upper_bin | gnomAD LoF decile bin |
| muts | Number of mutations in this protein in the gnomAD background |
| AF | gnomAD allele frequency |
| ExAC_MAF | ExAC allele frequency |
| GMAF | 1000 Genomes allele frequency |
| CLIN_SIG | ClinVar clinical significance |
| PolyPhen | PolyPhen2 category & score |
| SIFT | SIFT category & score |
| grantham | Grantham score |
| snv | SNV representation in genomic coordinates |
| sample_id | Sample ID(s) |
| uniprot | Uniprot ID |
| chr | chromosome |
| pos | chromosomal position |
| ref | reference allele |
| alt | alternate allele |
| enst | EnsEMBL transcript ID |
| strand | genomic strand |
| fold | AlphaFold protein segment |
| len | AlphaFold structure length |
| ph | Maestro (input) pH value |
| chain | Maestro (input) protein chain |
| score | Maestro score |
| delta_score | Maestro delta score |
| ddg_conf | Maestro ΔΔG confidence |
| msvu.chr | missense table lookup chromosome |
| msvul.pos | missense table lookup position |
| msvul.ref | missense table lookup reference allele |
| msvul.alt | missense table lookup alternate allele |
| AN | gnomAD allele number |
| AC | gnomAD (alt) allele count |
| Hom | gnomAD homozygous count |
| SOMATIC | gnomAD somatic annotation |
| af_len | AlphaFold structure length |
| bin | gnomAD LoF decile bin |
| low_ddG | number of low ΔΔG background variants in this gene |
| high_ddG | number of high ΔΔG background variants in this gene |
| q25_uniprot | lower quartile of ΔΔG in background variants within this gene |
| median_uniprot | median ΔΔG in background variants within this gene |
| q75_uniprot | upper quartile of ΔΔG in background variants within this gene |
| mean_uniprot | median ΔΔG in background variants within this gene |
| sd_uniprot | standard deviation of ΔΔG in background variants within this gene |
| nlog10p | negative logarithm of StabilitySort p_value |
| gnomad | gnomAD link |

In addition to the fields above, gene-level annotations have been imported directly from the gnomAD 2.1.1 [pLoF Metrics by Gene TSV](https://gnomad.broadinstitute.org/downloads#v2-constraint) which is further described at its [Gene constraint page](https://gnomad.broadinstitute.org/help/constraint) and in the [paper](https://www.nature.com/articles/s41586-020-2308-7#Sec12)'s supplementary data download.


## Maestro output table (for combined SNVs)

| Column Name | Description |
| ------ | ------ |
| pdb | Uniprot ID |
| aa_len | protein amino-acid length |
| pH | input pH value |
| chain | protein chain |
| score | Maestro score |
| delta_score | Maestro delta score |
| ddg | Maestro ΔΔG value |
| ddg_conf | Maestro ΔΔG confidence |
