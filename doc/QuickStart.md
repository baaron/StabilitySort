# Quick Start Guide

Welcome to StabilitySort, a fast tool for detecting potentially destabilising missense SNVs in your VCF datasets.

There are three main pages or views in the web application:

## 1. Load variants

This is where you can upload a VCF 4 file of up to 900MB in size, optionally bgzipped.

![Load view](../www/StartPage.png?raw=true)

There are also filters that you may apply at this stage to shorten your list of variants seen.

![Load view](../www/Filters.png?raw=true)

* Optionally restrict variants to only functionally constrained genes (gnomAD bins 0 to 2, see [here](https://www.nature.com/articles/s41586-020-2308-7))
* Set maximal gnomAD allele frequency threshold
* Set excluded ΔΔG range filter
* Set minimal Z-score threshold

Upon completion of the file upload, the VCF is compressed (if it isn't already) and tabix-indexed for "bcftools csq" analysis. An interim "file timestamp table view" may be seen as the VCF is being compressed, indexed and parsed in its temporary directory (which will be deleted upon the end of the interactive session). Do not worry that the file seems to be renamed to 0.vcf or 0.gz automatically -- this is merely a security side-effect of R Shiny's fileInput() control and will have no bearing on its uploaded content.

![Upload processing](../www/FileProcessing.png?raw=true)

Once the "bcftools csq" analysis is completed in the background, the application will automatically switch to the next (Missense) view.

Alternatively, users may click on the NA12878/ClinVar examples, or come in through a unique "dataset" link that they have saved from earlier sessions, in which case they will immediately be placed in the Missense view.

## 2. Missense View

This is the primary interactive view detailing the results of StabilitySort on their chosen/uploaded VCF data.

![Missense view](../www/MissenseView.png?raw=true)

### Functionality:

* Users may select a single row anywhere within this paginated table, which would bring up the selected missense SNV's ΔΔG (vertical red line) against a ΔΔG histogram of all other missense SNVs within the same gene in the default background set (currently gnomAD 3.1.2, but others are planned, e.g. 1kG, SGDP, MGRB)
* The table is sortable via the small up and down triangles next to the column labels (which are described in the [Glossary](ColumnNames.md))
    + to sort by more than one column, keep the shift key down while clicking on the sort triangles
* Each column has a filter box at the bottom which turns into a slider for numeric columns, or text search box for text columns. This allows users to narrow down the results they would like to see.
* The "Column visibility" box on the upper left allows users to show or hide columns as they see fit.
* The interactive scatterplot below the table has modifiable axes and colourscales in order to visualise the SNV table more flexibly.
![Scatter plot](../www/ScatterPlot.png?raw=true)
    + Hover over any of the points to get further SNV details
    + Clicking it would bring up its gnomAD page in a new window
    + To revisualise the plot with a new variable (e.g. PolyPhen score), the Z-axis can also be selected
![Scatter plot 3D](../www/ScatterPlot3.png?raw=true)
* The "Search" box on the upper right allows users to search the entire table for particular strings (e.g. BRCA1, NMUR2)
![Search box](../www/SearchBox.png?raw=true)

## 3. Combined SNV Effect

* When a row has been selected (in grey), and if the user would like to find out the combined nett ΔΔG value for all the mutations in this protein/gene (to determine if the overall mutational effect on that gene for that VCF sample is stabilising or destabilising), simply click the "Combined ΔΔG in selected gene" button on the upper left, which would proceed to the next view.

![Combined effect](../www/CombinedView.png?raw=true)
* The upper table shows all missense mutations in the selected gene (corresponding to the row selected in the previous view).
* Maestro is executed on-the-fly and its output is given as the lower table. The most informative column is the ddg (ΔΔG) column which gives a prediction of the overall destabilising effect of the combined missense SNVs on this protein (which in this case, has a combined destabilising effect with ΔΔG of 1.44).
* Users may return to the previous view via their browser's "back" button or right-click option, or start over by clicking the title logos at the top and bottom.
