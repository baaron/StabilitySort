<!-- PROJECT LOGO -->

[![Product Name Screen Shot][product-screenshot]](https://stabilitysort.org)

# Stability Sort: detect instability

<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->

[![MIT License][license-shield]][license-url]

<!-- TABLE OF CONTENTS -->

## Table of Contents

* [About the Project](#about-the-project)
  * [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
* [Usage](#usage)
* [Roadmap](#roadmap)
* [Contributing](#contributing)
* [License](#license)
* [Contact](#contact)
* [Acknowledgements](#acknowledgements)

<!-- ABOUT THE PROJECT -->

## About The Project

This tool aims to assist in pathogenic mutation prioritisation through the integration of protein stability measures from tools such as [MAESTRO](https://pubmed.ncbi.nlm.nih.gov/25885774/) applied to [AlphaFold](https://www.nature.com/articles/s41586-021-03819-2) predicted structures of all human proteins.

The first version of this tool has been published as a Bioinformatics Application Note at [https://doi.org/10.1093/bioinformatics/btac465](https://doi.org/10.1093/bioinformatics/btac465), and its backend has been replaced recently to further enhance performance for massively-parallel processing.

### Built With
StabilitySort is built with a multi-modal approach: as a web-accessible portal which allows users to manipulate prioritised variants from an uploaded [Variant Call Format](https://en.wikipedia.org/wiki/Variant_Call_Format) file, and also as a collection of scripts for more high-throughput analysis on modest computational hardware, and should work in principle on any [POSIX](https://en.wikipedia.org/wiki/POSIX#POSIX-oriented_operating_systems)-compatible operating system (though it has only been thoroughly tested on [Linux](https://linux.org) [Ubuntu](https://ubuntu.com) 16 through 20).

<!-- GETTING STARTED -->
## Getting Started
If you simply with to use the tool, a demo test instance is accessible [here](https://stabilitysort.org/app/). Please [email](mailto:aaron.chuah@anu.edu.au) if there are any access issues.

You may also find the [Quick Start Guide](doc/QuickStart.md) and [Glossary](doc/ColumnNames.md) useful.

The remaining instructions below are for users who wish to install Stability Sort locally on a server in order to interrogate their own private data without the risk of sending entire VCFs across the internet. Data bundles are being prepared for this to be hosted locally.

### Prerequisites
Stability Sort is built on:
* [R](https://www.r-project.org/) [Shiny](https://shiny.rstudio.com/), with optional deployment on [RStudio Server](https://www.rstudio.com/products/rstudio/#rstudio-server)
* [DuckDB](https://duckdb.org/), a multi-threaded [performant](https://duckdb.org/2021/12/03/duck-arrow.html) in-process OLAP column-store
  * directly accessing data within [ZSTD](http://facebook.github.io/zstd/)-compressed Apache [Parquet](https://parquet.apache.org/) files
* [BCFtools](https://samtools.github.io/bcftools/), an efficient VCF manipulation tool based on [HTSlib](http://www.htslib.org/download/)

Please follow the documentation for each of the packages above to install them on your relevant operating system.

#### Public genomic data
Stability Sort also depends on several files from publically-available human datasets which currently do not need to be downloaded as we will be providing a database resource bundle that has collated information from all these files:
* AlphaFold predicted 3D structures for the human proteome (UP000005640_9606_HUMAN.tar, 4.8G) from [https://alphafold.ebi.ac.uk/download](https://alphafold.ebi.ac.uk/download)
* GRCh38 human reference FASTA sequence and GFF3 annotation from [http://ensembl.org/Homo_sapiens](http://ensembl.org/Homo_sapiens)
* gnomAD 3.1.2  chromosomal VCFs from [https://gnomad.broadinstitute.org/downloads#v3-variants](https://gnomad.broadinstitute.org/downloads#v3-variants)
* gnomAD 2.1.1 pLoF Metrics by Gene TSV (gnomad.v2.1.1.lof_metrics.by_gene.txt.bgz, 4.4M) from [https://gnomad.broadinstitute.org/downloads#v2-constraint](https://gnomad.broadinstitute.org/downloads#v2-constraint)

A [data bundle](https://stabilitysort.org/download/) has been prepared to share all precomputed stability values across the human proteome in tabular form for analysis with other tools.

StabilitySort is designed to be dual-modal software: directly accessible online at:
[https://stabilitysort.org/app/](https://stabilitysort.org/app/)
or locally on any POSIX-operating system (the scripts are functional and produce identical ouput to the web app).
A detailed user-guide for the Command-Line-Interface version of StabilitySort is being written.

<!-- USAGE EXAMPLES -->
## Usage

Please read the [Quick Start Guide](doc/QuickStart.md) for an easy introduction to Stability Sort. 

<!-- ROADMAP -->
## Roadmap

See the [open issues](https://gitlab.com/baaron/StabilitySort/issues) for a list of proposed features (and known issues).


<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**. See [CONTRIBUTING.md](CONTRIBUTING.md) for more information.


<!-- LICENSE -->
## License
Distributed under the MIT License. See [LICENSE](LICENSE) for more information.


<!-- CONTACT -->
## Contact
* [Aaron Chuah](https://orcid.org/0000-0003-3066-701X) - aaron.chuah@anu.edu.au <!-- [![LinkedIn][linkedin-shield]][linkedin-url] -->
* [T Daniel Andrews](https://orcid.org/0000-0003-3922-6376) - dan.andrews@anu.edu.au, [Genome Informatics Laboratory][product-url]

<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements
* [The John Curtin School of Medical Research](https://jcsmr.anu.edu.au/)
* [The Australian National University](https://www.anu.edu.au)
* [National Computational Infrastructure](https://nci.org.au)

[![GIL.JCSMR][product-banner]][product-url]

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=flat-square
[license-url]: https://gitlab.com/baaron/StabilitySort/-/blob/main/LICENSE
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=flat-square&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/chuah-aaron/
[product-url]: https://jcsmr.anu.edu.au/research/groups/andrews-group-genome-informatics
[product-screenshot]: www/SSlogo.png
[product-banner]: www/JCSMR.png
