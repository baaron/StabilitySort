#!/usr/bin/env perl
while(<>) {
#	HG01413 10      93943   missense        TUBB8   ENST00000447903 protein_coding  -       58L>58P 93943A>G
#CSQ	CPIEEU_sample10_sg1_humansingle1	2	1	11090897	missense	MASP2	ENST00000400897	protein_coding	-	377V>377A	11090897A>G
	chomp;
	my @line=split/\t/;
	my @prot_mut=$line[9]=~/\d+([A-Y])\>(\d+)([A-Y])$/;
	if(@prot_mut) {
		$line[9]=join"\t",@prot_mut;;	
	}else {
		print STDERR "@line\n";
		$line[9]="\t\t";
	}
	$line[10]=join"\t",$line[10]=~/\d+([A-Y])\>([A-Y])$/;
	print join("\t",@line[0..3],@line[5..$#line]),"\n";
}
