#!/bin/bash
REF_DIR=/data/maestro/data
VCF_FILE=$1
OUT_FILE=$REF_DIR/out.$2.csq.hom.tsv.gz
if [[ ! -e $OUT_FILE ]]; then
	if [[ $(file -b --mime-type "$1") != 'application/gzip' ]]; then
		bgzip -c $VCF_FILE > ${VCF_FILE}.gz
		VCF_FILE=${VCF_FILE}.gz
	fi
	if [[ ! -e $VCF_FILE.tbi ]]; then
		tabix -p vcf $VCF_FILE
	fi
#	bcftools csq -f $REF_DIR/hs37d5.fa.gz -g $REF_DIR/Homo_sapiens.GRCh37.87.gff3.gz $VCF_FILE -p a -O t | grep missense | cut -f2,4-6 | tr '|' '\t' | sort -k2,2n -k3,3n -k6,6 | uniq | $REF_DIR/splitHGVS.pl | pigz -c > $OUT_FILE
	bcftools csq -f $REF_DIR/hs37d5.fa.gz -g $REF_DIR/Homo_sapiens.GRCh37.87.gff3.gz $VCF_FILE -p a -O t | grep missense | cut -f2-6 | tr '|' '\t' | grep -f /data/alphafold/human.enst.tsv | $REF_DIR/splitHGVShom.pl | pigz -c > $OUT_FILE
fi
